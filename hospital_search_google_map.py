from selenium import webdriver
from elasticsearch import Elasticsearch
import time
##import requests
import json
import datetime
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

try:
  es = Elasticsearch(['http://localhost:1414'])
  #print ("Connected", es.info())
except Exception as ex:
  #print ("Error:", ex)
  pass


final_info=[]
not_found=[]
import time
start=time.time()

def scrape_info(browser, country):  
    hospital_info=[]
    time.sleep(2)
    hospitals=browser.find_elements_by_xpath('//div[@role="listitem"]')
    #print(hospitals[0].text)
    for hospital in hospitals:
        info={}
        try:
            info["hospital_name"]=hospital.find_element_by_xpath('.//h3[@class="section-result-title"]/span').text
        except:
            pass
        try:
            info["hospital_type"]=hospital.find_element_by_xpath('.//span[@class="section-result-details"]').text
        except:
            pass
        try:
            info["hospital_address"]=hospital.find_element_by_xpath('//span[@class="section-result-location"]').text
        except:
            pass
        try:
            info["hospital_link"]=hospital.find_element_by_xpath('.//a[@class=" section-result-action section-result-action-wide"]').get_attribute('href')
        except:
            pass
##        print(info)
        #hospital_info.append(info)
        with open(country+'_hospital_info'+'.json', 'a') as outfile:
            json.dump(info, outfile, indent=2)



def next_pages(browser, country):
    next_page=browser.find_element_by_xpath('//*[contains(@id,"section-pagination-button-next")]/span')
    try:
        # time.sleep(2)
        next_page.click()
        time.sleep(6)
        scrape_info(browser, country)
    except:
        pass

def single_result(browser, country):
    info={}
    try:
        info["hospital_name"]=browser.find_element_by_xpath('//*[@id="pane"]/div/div[1]/div/div/div[2]/div[1]/div[1]/h1').text
    except:
        pass
    try:
        info["hospital_type"]=browser.find_element_by_xpath('//*[@id="pane"]/div/div[1]/div/div/div[2]/div[1]/div[2]/div/div[2]/span[1]/span[1]/button').text
    except:
        pass
    try:
        info["hospital_address"]=browser.find_element_by_xpath('//*[@id="pane"]/div/div[1]/div/div/div[8]/div/div[1]/span[3]/span[3]').text
    except:
        pass
    try:
        info["hospital_link"]=browser.find_element_by_xpath('//*[@id="pane"]/div/div[1]/div/div/div[10]/div/div[1]/span[3]/span[3]').get_attribute('href')
    except:
        pass
    print(info)
    #hospital_info.append(info)
    with open(country+'_hospital_info'+'.json', 'a') as outfile:
        json.dump(info, outfile, indent=2)



def main():
    countries=["unitedstates"]
    for country in countries:
         
        with open(country+".json","r") as f:
            towns=json.load(f)
    ##    towns = ["anderlecht"]
        towns=towns
        hospital_info=[]
        browser = webdriver.Chrome(executable_path = r'C:\softwares\chromedriver_win32\chromedriver.exe')
        browser.get("https://www.google.com/maps/")
        for town in towns:
            try:
                WebDriverWait(browser, 30).until(EC.element_to_be_clickable((By.XPATH, '//input[@id="searchboxinput"]')))
                search_box=browser.find_element_by_xpath('//input[@id="searchboxinput"]')
                search_box.clear()
                search_box.send_keys("Hospitals in "+town+" "+country)
                search_box.send_keys(Keys.RETURN)
                time.sleep(4)
                scrape_info(browser, country)
                
            except:
                browser.quit()
                time.sleep(8)
                browser = webdriver.Chrome(executable_path = r'C:\softwares\chromedriver_win32\chromedriver.exe')
                browser.get("https://www.google.com/maps/")
                WebDriverWait(browser, 30).until(EC.element_to_be_clickable((By.XPATH, '//input[@id="searchboxinput"]')))
                search_box=browser.find_element_by_xpath('//input[@id="searchboxinput"]')
                search_box.clear()
                search_box.send_keys("Hospitals in "+town+" "+country)
                search_box.send_keys(Keys.RETURN)
                time.sleep(3)
                scrape_info(browser, country)

            try:
                new_total_link=browser.find_element_by_xpath('//*[@class="gm2-caption"]//span[2]').text
            except:
                new_total_link=None
            if new_total_link is not None:
                old_total_link=19
                if len(new_total_link) == 0:
                  new_total_link= 0
                while int(new_total_link) > old_total_link:
                    print("oldlink ",old_total_link," newlinks ", new_total_link)
                    old_total_link=int(new_total_link)
                    next_pages(browser, country)
                    new_total_link=browser.find_element_by_xpath('//*[@class="gm2-caption"]//span[2]').text

            try:
                hospital_name=browser.find_element_by_xpath('//*[@id="pane"]/div/div[1]/div/div/div[2]/div[1]/div[1]/h1').text
            except:
                hospital_name=None

            if hospital_name is not None:
                single_result(browser, country)


main()

